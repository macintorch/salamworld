//
//  SalamViewController.swift
//  SalamWorld
//
//  Created by Ainor Syahrizal on 28/06/2016.
//  Copyright © 2016 Ainor Syahrizal. All rights reserved.
//

import UIKit

class SalamViewController: UIViewController {

    // instance properties
    var helloLabel: UILabel!
    var hiButton: UIButton!
    var byeButton: UIButton!
    
    //set up lifecycle
    //viewDidLoad()
    //didReceiveMemoryWarning()
    //Navigation
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // set the background color to lightgraycolor
        self.view.backgroundColor = UIColor.lightGrayColor()
        
        // create a label and pass it to frame
        //UILabel is a subclass of UIView
        helloLabel = UILabel(frame: CGRect(x: 50, y: 60, width: 200, height: 30))
        
        //set the background
        helloLabel.backgroundColor = UIColor.whiteColor()
        
        //make a text
        helloLabel.text = "Salam World"
        
        // view is a subclass of UIView
        //set the helloLabel as a subview
        self.view.addSubview(helloLabel)
        
        // create hi button
        hiButton = UIButton(frame: CGRect(x: 40, y: 250, width: 100, height: 30))
        
        // set the button to Hi!
        hiButton.setTitle("Hi!", forState: UIControlState.Normal)
        
        
        //set hiButton (self) as the target, action is what does it suppose to do? call sayhi() , after user's finger lift up the button
        hiButton.addTarget(self, action: #selector(SalamViewController.sayhi), forControlEvents: UIControlEvents.TouchUpInside)
        
        //set the hiButton as subview
        self.view.addSubview(hiButton)
        
        
        // bye button
        byeButton = UIButton(frame: CGRect(x: 200, y: 250, width: 100, height: 30))
        
        byeButton.setTitle("Good Bye!", forState: UIControlState.Normal)
        
        byeButton.addTarget(self, action: #selector(SalamViewController.saybye), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(byeButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //instance methods
    
    func sayhi()  {
        
        print("Say hi now")
        
        //change the "Salam World" label to "Say hi now"
        helloLabel!.text = "Say Hi Now!"
    }
    
    func saybye()  {
        print("Good bye now")
        //change the "Salam World" label to "Good bye now"
        helloLabel.text = "Good bye now"
        
    }
    
}
